<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Properti;
use App\Models\Kamar;

class PropertiController extends Controller
{
    public function listProperti(){
        $dataProperti = Properti::select('properti.id', 'properti.nama_properti', 'properti.jumlah_properti','kamar.nama_kamar')
        ->join('kamar', 'kamar.id', '=', 'properti.id_kamar')->get();
        return view('properti.list', compact('dataProperti'));
    }
    public function formInput(){
        $dataKamar = Kamar::select('id', 'nama_kamar')->get();
        return view('properti.form', compact('dataKamar'));
    }

    public function simpanData(Request $req){
        try{
            $data = $req->all();
            $save = new Properti;
            $save->nama_properti = $data['nama_properti'];
            $save->jumlah_properti = $data['jumlah_properti'];
            $save->id_kamar = $data['id_kamar'];
            $save->save();
            return redirect()->route('properti.list')->with('sucses', _('berhasil'));
        }catch (\Throwable $th) {
            return redirect()->route('properti.list')->with('error', __($th->getMessage()));
        }
    }
    public function deleteData($id){
        try{
            Properti::where('id', $id)->delete();

            return redirect()->route('properti.list')->with('sucses', _('berhasil menghapus'));
        }catch (\Throwable $th) {
            return redirect()->route('properti.list')->with('error', __($th->getMessage()));
        }
    }
}
