<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kamar;

class KamarController extends Controller
{
    public function listKamar(){
        $dataKamar = Kamar::select('id', 'nama_kamar', 'kapasitas')->get();
        return view('kamar.list', compact('dataKamar'));
    }

    public function formInput(){
        return view('kamar.form_input');
    }

    public function simpanData(Request $req) {
        try {
            $datas = $req->all();
            $save = new Kamar;
            $save->nama_kamar = $datas['nama_kamar'];
            $save->kapasitas = $datas['kapasitas'];
            $save->save();
            return redirect()->route('kamar.list')->with('suscces', _('berhasil'));
        }  catch (\Throwable $th) {
            return redirect()->route('kamar.list')->with('error', __($th->getMessage()));
        }
    }

    public function formEdit($id){
        $getDataById = Kamar::select('id', 'nama_kamar', 'kapasitas')->where('id', $id)->first();
        return view('kamar.form_edit', compact('getDataById'));
    }

    public function editData(Request $req, $id){
        try {
            $datas = $req->all();
            Kamar::where('id', $id)->update([
                'nama_kamar' => $datas['nama_kamar'],
                'kapasitas' => $datas['kapasitas']
            ]);
            return redirect()->route('kamar.list')->with('succsess', __('berhasil edit data'));
        } catch (\Throwable $th) {
            return redirect()->route('kamar.form-edit', $id)->with('error', __($th->getMessage()));
        }
    }

    public function deleteData($id){
        try {
             Kamar::where('id', $id)->delete();

            return redirect()->route('kamar.list')->with('succsess', __('berhasil edit data'));
        } catch (\Throwable $th) {
            return redirect()->route('kamar.list', $id)->with('error', __($th->getMessage()));
        }
    }
 }

