<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengguna;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function formRegister()
    {
        return view('pengguna.form_register');
    }

    public function simpanRegister(Request $req)
    {
        try {
            $this->validate($req, [
                'nama' => 'required|string|max:255',
                'username' => 'required|max:255',
                'email' => 'required|string|email|unique:pengguna',
                'password' => 'required|string|min:8',
            ]);

            $datas = $req->all();
            $save = new Pengguna;
            $save->nama = $datas['nama'];
            $save->username = $datas['username'];
            $save->level = $datas['level'];
            $save->email = $datas['email'];
            $save->password = Hash::make($datas['password']);
            $save->save();
            return redirect()->route('register.form-register')->with('suscces', __('berhasil'));
        } catch (\Throwable $th) {
            return redirect()->route('register.form-register')->with('error', __($th->getMessage()));
        }
    }
}
