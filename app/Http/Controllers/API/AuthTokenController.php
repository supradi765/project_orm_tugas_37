<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AuthToken;
use Illuminate\Support\Str;

class AuthTokenController extends Controller
{
    function token()
    {
        $access_token = Str::random(54);
        AuthToken::insert([
            'access_token' => $access_token,
            'token_type' => 'Bearer',
            'expires_at' => date('Y-m-d H:i:s', strtotime('+3600 seconds')),
            'scope' => 'resource.WRITE resource.READ'
        ]);

        return response()->json([
            'access_token' => $access_token,
            'token_type' => 'Bearer',
            'expires_in' => '3600',
            'scope' => 'resource.WRITE resource.READ'
        ], 200);
    }
}
