<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kamar;


class KamarController extends Controller
{
    function listData()
    {
        $dataReturn = Kamar::get();
        return response()->json([

            'message' => 'success',
            'data' =>  $dataReturn

        ], 200);
    }

    function simpanData(Request $req)
    {
        try {
            $datas = $req->all();
            $save = new Kamar;
            $save->nama_kamar = $datas['nama_kamar'];
            $save->kapasitas = $datas['kapasitas'];
            $save->save();
            return response()->json([
                'status'  => true,
                'status_code'  => 201,
                'message'  => 'berhasil membuat data',
                'data'  => null,
            ], 201);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'status_code' => 500,
                'message' => 'Somthing wont wrong, please try again later',
                'data'  => $th,
            ], 500);
        }
    }
}
