<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\AuthToken;

class AuthTokenVerif
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $header = $request->header('Authorization');
        $input = explode(" ", $header);

        if (strtolower($input[0]) == 'bearer') {
            $access_token = $input[1];
            $token = authToken::where('access_token', $access_token)
                ->where('expires_at', '>=', date('Y-m-d H:i:s'))->first();

            if ($token) {
                return $next($request);
            }
        }
        return response()->json([
            'status' => false,
            'status_code' => 401,
            'message' => 'Unauthorized'
        ], 401);
    }
}
