<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $AUTH_USER = env('BASIC_AUTH_USERNAME', 'SiswaPintarSukses');
        $AUTH_PASS = env('BASIC_AUTH_PASSWORD', 'SatuTeamTujuan');
        header('Cache-Control: no-chache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $is_not_authenticated = (!$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW'] != $AUTH_PASS
        );
        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('www-Authenticate: Basic realm="Acces denied" ');
            return response()->json([
                'status' => false,
                'status_code' => 401,
                'message'  => 'Unauthorized'
            ], 401);
        }
        return $next($request);
    }
}
