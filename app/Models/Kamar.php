<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Kamar extends Model
{
    use HasFactory;
    use softDeletes;

    protected $table = "kamar";

    public $timestamps = true;
     protected $fillable = [
        'nama_kamar',
        'kapasitas'
     ];
}
