<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthToken extends Model
{
    use HasFactory;

    protected $table = "auth_token";
    public $timestamp = true;

    protected $fillable =
    [
        'access_token',
        'token_type',
        'expires_at',
        'scope'
    ];
}
