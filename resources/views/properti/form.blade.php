
<html>
    <head>
        <title>Form Input Properti</title>
    </head>
    <body>
        <h1>Form Input Properti</h1>
        @if (session('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('error') }}
        </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('success') }}
            </div>
        @endif
        <form action="{{ route('properti.simpan-data') }}" method="POST">
        @csrf
            <label>Nama Properti</label><br>
            <input type="text" name="nama_properti" id="nama_properti" value="{{ old('nama_properti') }}" /><br>
            <label>Jumlah</label><br>
            <input type="text" name="jumlah_properti" id="jumlah_properti" value="{{ old('jumlah_properti') }}" /><br>
            <label>Nama kamar</label><br>
            <select name="id_kamar" id="id_kamar">
                <option value="">Pilih Kamar</option>
                @foreach ($dataKamar as $row)
                    <option value="{{ $row->id }}">{{ $row->nama_kamar }}</option>
                @endforeach
            </select>
            <br>
            <input type="submit" value="Simpan" />
            <a href="{{ route('properti.list') }}" >Kembali</a>
        </form>
    </body>
</html>
