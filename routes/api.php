<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\KamarController;
use App\Http\Controllers\API\AuthTokenController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(KamarController::class)->name('kamar')->middleware('AuthTokenVerif')->prefix('kamar')->group(function () {
    Route::get('/', 'listData')->name('index');
    Route::post('/simpan', 'simpanData')->name('simpan');
});

Route::controller(AuthTokenController::class)->name('api.token')->middleware('BasicAuth')->prefix('/token')->group(function () {
    Route::get('/', 'token')->name('token');
});
